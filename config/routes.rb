Rails.application.routes.draw do
  

  get 'welcome_calculator/show'

 scope controller: 'subtract' do
    get 'subtract/form', action: 'form', as: 'subtract_form'
    post '/subtract/result' => 'subtract#result', as: 'subtract_result'
  end
  
  scope controller: 'add' do
    get 'add/form', action: 'form', as: 'add_form'
    post 'add/result'
  end
  
  scope controller: 'multiply' do
    get 'multiply/form', action: 'form', as: 'multiply_form'
    post 'multiply/result'
  end

  scope controller: 'divide' do
    get 'divide/form', action: 'form', as: 'divide_form'
    post 'divide/result'
  end
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'welcome_calculator#show'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
